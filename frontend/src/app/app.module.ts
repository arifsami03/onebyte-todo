import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { BaseInterceptor } from "@app/helpers/base.interceptor";

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import '../assets/styles';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RouterModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: BaseInterceptor, multi: true}
      ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule { }