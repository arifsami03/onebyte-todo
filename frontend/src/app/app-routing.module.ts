import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard, UserStateGuard } from '@app/helpers';

const appRoutes: Routes = [

    {
        path: 'todo',
        canActivate: [AuthGuard],
        loadChildren: './modules/todo/todo.module#TodoModule'
    },
    {
        path: 'auth',
        canActivate: [UserStateGuard],
        loadChildren: './modules/auth/auth.module#AuthModule'
    },
    { path: '', redirectTo: 'auth', pathMatch: 'full' },
    { path: '**', redirectTo: 'auth' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }