import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

/**
 * Check if cookie exists, then navigate to /todo/list
 */
@Injectable({ providedIn: 'root' })
export class UserStateGuard implements CanActivate {

    constructor(
        private router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (document.cookie)
            this.router.navigate(['/todo/list']);
        else
            return true;
    }

}
