// Import core and third party modules needed for layout
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule, MatButtonModule, MatCardModule, MatListModule, MatIconModule } from "@angular/material";

// Import components
import { TodoComponent, HeaderComponent, HomeComponent, TodoListComponent } from './components';

export const __IMPORTS = [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatIconModule
];

export const __DECLARATION = [
    TodoComponent,
    HeaderComponent,
    HomeComponent,
    TodoListComponent
];
