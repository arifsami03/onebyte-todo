import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '@environments/environment';


@Injectable({ providedIn: 'root' })
export class TodoService {

    private apiUrl = environment.API_BASE_URL.concat('todo');

    constructor(
        private http: HttpClient
    ) { }

    getList(): Observable<any> {
        return this.http.get(`${this.apiUrl}`).pipe(
            tap(data => data as any),
            catchError(this.errorHandler)
        );
    }

    create(todo: any): Observable<any> {
        return this.http.post(`${this.apiUrl}`, todo).pipe(
            tap(data => data as any),
            catchError(this.errorHandler)
        );
    }

    private errorHandler(httpErrorResponse: HttpErrorResponse) {
        return throwError(httpErrorResponse.error || 'Server error');
    }

}
