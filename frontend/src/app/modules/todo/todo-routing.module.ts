import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TodoComponent, HomeComponent, TodoListComponent } from './components';

const todoRoutes: Routes = [
    {
        path: '', component: TodoComponent,
        children: [
            { path: 'home', component: HomeComponent },
            { path: 'list', component: TodoListComponent }
        ]
    },
    { path: '', redirectTo: 'list', pathMatch: 'full' },

];

@NgModule({
    imports: [
        RouterModule.forChild(todoRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class TodoRoutingModule { }