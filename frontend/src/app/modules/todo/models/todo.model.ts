import { FormControl, Validators } from '@angular/forms';

export class TodoModel {

    static attributesLabels = {
        todo: 'Todo',
        completed: 'Completed',
        categories: 'Categories'
    };

    public todo: string;
    public completed: boolean;
    public categories: string[];

    constructor() { }

    /**
     * Form Validation Rules
     */
    public validationRules() {
        return {
            todo: new FormControl('', Validators.required),
            completed: new FormControl(false),
            categories: new FormControl('')
        };
    }

}
