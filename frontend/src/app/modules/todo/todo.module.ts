import { NgModule } from '@angular/core';
import { TodoRoutingModule } from "./todo-routing.module";
import { __IMPORTS, __DECLARATION } from "./components.barrel";

@NgModule({
    imports: [__IMPORTS, TodoRoutingModule],
    declarations: [__DECLARATION]
})
export class TodoModule { }
