import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TodoService } from '@app/modules/todo/services';
import { TodoModel } from '@app/modules/todo/models';

@Component({
    selector: 'todoApp-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})

export class TodoListComponent implements OnInit {

    public loaded: boolean = false;
    public todoList: TodoModel[];

    constructor(
        private router: Router,
        private todoService: TodoService
    ) { }

    ngOnInit() {

        this.todoService.getList().subscribe(res => {
            if (res && res['status'] === 401)
                this.router.navigate(['/auth']);
            else {
                this.todoList = res;
                this.loaded = true;
            }
        }, error => {
            this.todoList = [new TodoModel()];
            this.loaded = true;
        });
    }
}