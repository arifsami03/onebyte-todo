import { Component } from '@angular/core';

@Component({
    selector: 'todoApp-todo',
    templateUrl: './todo.component.html',
    styleUrls: ['./todo.component.scss']
})

export class TodoComponent {}