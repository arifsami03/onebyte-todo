import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { GLOBAL_CONSTANTS } from "@constants/constant";
@Component({
    selector: 'todoApp-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
    public appTitle = GLOBAL_CONSTANTS.APP_TITLE;

    constructor(private router: Router) { }

    logout() {

        console.log('logot')
        document.cookie = "cookie=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        console.log('document.cookie: ', document.cookie);
        this.router.navigate(['/auth']);
    }
}
