import { NgModule } from '@angular/core';

import { AuthRoutingModule } from './auth-routing.module';

import { __IMPORTS, __DECLARATION } from "./components.barrel";

@NgModule({
    imports: [__IMPORTS, AuthRoutingModule],
    declarations: [__DECLARATION]
})

export class AuthModule { }
