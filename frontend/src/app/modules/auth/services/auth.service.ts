import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '@environments/environment';


@Injectable({ providedIn: 'root' })
export class AuthService {

    private apiUrl = environment.API_BASE_URL.concat('auth');

    constructor(
        private http: HttpClient
    ) { }

    login(user: any): Observable<any> {
        return this.http.post(`${this.apiUrl}/login`, user).pipe(
            tap(data => data as any),
            catchError(this.errorHandler)
        );
    }

    signup(user: any): Observable<any> {
        return this.http.post(`${this.apiUrl}/signup`, user).pipe(
            tap(data => data as any),
            catchError(this.errorHandler)
        );
    }

    private errorHandler(httpErrorResponse: HttpErrorResponse) {
        return throwError(httpErrorResponse.error || 'Server error');
    }


}
