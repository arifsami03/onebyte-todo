import { FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

export class AuthModel {

    static attributesLabels = {
        name: 'Name',
        email: 'Email',
        password: 'Password',
        confirmPassword: 'Confirm Password',
    };

    public name: string;
    public email: string;
    public password: string;
    public confirmPassword: string;

    constructor() { }

    /**
      * Match password with the given control
      * @param equalControlName
      */
    public matchPassword(equalControlName: string): ValidatorFn {
        return (
            control: AbstractControl
        ): {
            [key: string]: any;
        } => {
            if (!control['_parent']) return null;

            if (!control['_parent'].controls[equalControlName]) throw new TypeError('Form Control ' + equalControlName + ' does not exists.');

            let controlMatch = control['_parent'].controls[equalControlName];

            // cehecking the field is dirty
            if (controlMatch.dirty === true) {
                if (controlMatch.value == control.value) {
                    controlMatch.setErrors(null);
                    return null;
                } else {
                    return { passNotMatched: true };
                }
            }

        };
    }

    /**
     * Signup Form Validation Rules
     */
    public signupValidationRules() {
        return {
            name: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', [
                Validators.required,
                this.matchPassword('confirmPassword')
            ]),
            confirmPassword: new FormControl('', [
                Validators.required,
                this.matchPassword('password')
            ])
        };
    }

    /**
     * Login Form Validation Rules
     */
    public loginValidationRules() {
        return {
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        };
    }
}
