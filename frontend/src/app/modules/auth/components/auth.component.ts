import { Component } from '@angular/core';

/**
 * This component is responsible to render default layout i.e header for public home,
 * signup and login page.
 * It also provides the router-outlet for other components to render here
 */
@Component({
    selector: 'todoApp-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent { }