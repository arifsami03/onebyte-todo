import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { AuthService } from '@app/modules/auth/services';
import { AuthModel } from '@app/modules/auth/models';
import { GLOBAL_CONSTANTS } from "@constants/constant";

/**
 * This component is used to display signup or login form based on the provided pageMode parameter
 */
@Component({
    selector: 'todoApp-auth-form',
    templateUrl: './auth-form.component.html',
    styleUrls: ['./auth-form.component.scss'],
    providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],

})
export class AuthFormComponent implements OnInit {

    public pageMode: string = '';
    public pageTitle: string = '';
    public loaded: boolean = false;
    private authFormMode = GLOBAL_CONSTANTS.AUTH_FORM_MODE;
    public attributesLabels = AuthModel.attributesLabels;
    public fg: FormGroup;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private location: Location,
        private authService: AuthService
    ) {
        this.pageMode = this.activatedRoute.snapshot.data['pageMode'];
        this.pageTitle = this.activatedRoute.snapshot.data['pageTitle'];
    }

    /**
     * Initializes form group according to pageMode parameter 
     * Incase of signup pageMode, initializes formGroup with signupValidationRules and 
     * incase of login pageMode, initializes formGroup with loginValidationRules 
     */
    ngOnInit() {

        if (this.pageMode === this.authFormMode.SIGNUP)
            this.fg = this.fb.group(new AuthModel().signupValidationRules());
        else
            this.fg = this.fb.group(new AuthModel().loginValidationRules());

        this.loaded = true;
    }

    submit(formData: AuthModel) {
        if (this.pageMode === this.authFormMode.SIGNUP)
            this.authService.signup(formData).subscribe(res => {
                alert('Alert on development: Your account has been created successfully');
            }, error => {
                alert('Alert on development: Something went wrong, try again');
                console.log('result: ', error);
            });
        else
            this.authService.login(formData).subscribe(res => {
                this.router.navigate(['/todo/list']);
            }, error => {
                alert('Alert on development: Something went wrong, try again');
                console.log('result: ', error);
            });
    }

    cancel(): void {
        this.location.back();
    }
}