import { Component } from '@angular/core';

/**
 * This component provides the route button for signup or login forms.
 */
@Component({
    selector: 'todoApp-auth-body-content',
    templateUrl: './body-content.component.html',
    styleUrls: ['./body-content.component.scss']
})
export class AuthBodyContentComponent { }