// Import core and third party modules needed for layout
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule, MatButtonModule, MatFormFieldModule, MatInputModule } from "@angular/material";

// Import components
import { AuthComponent, AuthBodyContentComponent, AuthFormComponent } from './components';

export const __IMPORTS = [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule
];

export const __DECLARATION = [
    AuthComponent,
    AuthBodyContentComponent,
    AuthFormComponent
];
