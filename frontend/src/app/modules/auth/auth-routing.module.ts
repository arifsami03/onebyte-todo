import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthComponent, AuthBodyContentComponent, AuthFormComponent } from './components';

import { GLOBAL_CONSTANTS } from "@constants/constant";

const authRoutes: Routes = [
    {
        path: '', component: AuthComponent, children: [
            { path: '', component: AuthBodyContentComponent },
            {
                path: 'signup', component: AuthFormComponent, data: {
                    pageMode: GLOBAL_CONSTANTS.AUTH_FORM_MODE.SIGNUP,
                    pageTitle: 'Signup',
                }
            },
            {
                path: 'login', component: AuthFormComponent, data: {
                    pageMode: GLOBAL_CONSTANTS.AUTH_FORM_MODE.LOGIN,
                    pageTitle: 'Login'
                }
            }
        ]
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(authRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AuthRoutingModule { }