export const GLOBAL_CONSTANTS = {
    APP_TITLE: 'World\'s Best Todo App',
    AUTH_FORM_MODE: { // set and check the mode for auth form
        SIGNUP: 'signup',
        LOGIN: 'login'
    }
}