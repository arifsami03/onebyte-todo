
// Modules Start
const express = require('express');
const cors = require('cors');
const body_parser = require('body-parser');
const cookieParser = require('cookie-parser');
// Modules End


// Modules Configuration Start
const config = require('./config/config.json');

const corsOptions = {
    origin: 'http://localhost:4200',
    credentials: true,
    preflightContinue: true,
    maxAge: 24 * 60 * 60 * 1000
};

const app = express();
app.use(cors(corsOptions));
app.use(body_parser.json({ limit: '5mb' }));
app.use(body_parser.urlencoded({ limit: '5mb', extended: true }));
app.use(cookieParser(config.cookieSecret));

// Modules Configuration End

const routes = require('./src/routes/index.route');

app.use(config.apiPrefix, routes);

// Server Initialization Start
require('./server')(app);
// Server Initialization Start