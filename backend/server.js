const config = require('./config/config.json');

const mongoose = require('./src/loaders/mongoose-loader');
const http = require('http');

module.exports = app => {

  const server_port = process.env.PORT || config.server.port;

  http.createServer(app).listen(server_port, config.server.host, () => console.log(`Server running at http://${config.server.host}:${server_port}/`));
  mongoose();   // Mongo Initiate
};