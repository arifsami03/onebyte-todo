const UserModel = require('../models/user.model');

module.exports = {

    login(req, res) {

        UserModel.login(req.body.email, req.body.password)
            .then(result => {
                res.cookie('cookie', result.user._id, { signed: true });
                res.json(result.data);
            })
            .catch(error => res.json(error));
    },

    signup(req, res) {

        const user = {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
        };

        UserModel.signup(user)
            .then(savedUser => res.json(savedUser))
            .catch(error => res.json(error));
    }
}