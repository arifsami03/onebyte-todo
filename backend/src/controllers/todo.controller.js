const TodoModel = require('../models/todo.model');

module.exports = {

    findAll(req, res) {
        TodoModel.findAll()
            .then(result => res.json(result))
            .catch(error => res.json(error));
    },

    create(req, res) {

        const todo = req.body;

        TodoModel.create(todo)
            .then(savedTodo => res.json(savedTodo))
            .catch(error => res.json(error));
    }
}