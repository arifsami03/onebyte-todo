const config = require('./../../../../config/config.json');

const mongoose = require('mongoose');

module.exports = () => {

  mongoose.connect(config.mongoose.url, { useNewUrlParser: true, useCreateIndex: true }).then(
    () => {},
    err => console.log(`Unable to connect to database at${config.mongoose.url}\\n${err}`)
  );

};