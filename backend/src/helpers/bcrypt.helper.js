const bcrypt = require('bcrypt-nodejs');
const config = require('./../../config/config.json');

module.exports = {

    bcryptPassword: (password) => {
        return new Promise((resolve, reject) => {
            bcrypt.genSalt(config.bcryptSaltRounds, (err, salt) => {
                if (err) return reject(err);
                bcrypt.hash(password, salt, null, (err, hash) => {
                    if (err) return reject(err);
                    return resolve(hash);
                });
            });
        });
    },

    bcryptPassCompare: (password, hash) => {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, hash, (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
}