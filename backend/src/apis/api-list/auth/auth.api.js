const nodeconfig = require(`__basedir/${node-config.json}`);
const resmessage = require(`__basedir/json-list/${res-message.json}`);
const routes = require('./routes');

const jwt = require('jsonwebtoken');

// Custom Modules
const bcrypt_module = require('bcrypt-module');
const response_parser_module = require('response-parser-module');
const res_message_builder_module = require('res-message-builder-module');
// Custom Modules

// Loaders
const schema_loader = require('@loaders/schema-loader');
// Loaders

const $user = schema_loader('user');

module.exports = app => {

  app.post(`/${routes.register}`, (req, res) => {

    const user = new $user(req.body);

    user.save(errors => {

        if(!errors) response_parser_module({
          status: 200,
          message: resmessage.user.register,
          data: exclude_include_keys_module({
            obj: stringify_mongo_module(user.toObject()),
            keys: ['created_at', 'updated_at'],
            type: 'exclude'
          }),
          res
        })
      }
    )

  });

  app.post(`/${routes.login}`, (req, res) => {

    $user.findOne(
      {email: req.body.email},
      {},
      {lean: true},
      (res_err, res_user) => {
        if (res_err|| !res_user) return response_parser_module({status: 403, message: resmessage.auth.fail, res});

        bcrypt_module.compare(req.body.password, res_user.password, (err, pass) => {

          if (err || !pass) return response_parser_module({status: 403, message: resmessage.auth.fail, res});

          res_user.token = jwt.sign({id: res_user._id}, nodeconfig.hash_key);
          delete res_user.password;

          return response_parser_module({status: 200, message: resmessage.auth.success, data: res_user, res});
        })

      }
    )
  });

};
