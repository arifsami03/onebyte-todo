
const config = require('../../');

module.exports = (req, res, next) => {

    if (!jwtexceptionlist.includes(req.path.split('/v1')[1])) {

      if (req.method === 'OPTIONS') return res.sendStatus(204);

      jwt.verify(req.headers.authorization || '', config.hash_key, (err, jwt_parsed) => {

        if (err || !jwt_parsed) return response_parser_helper({status: 401, message: err.toString(), res});

        $user.findOne(
          {_id: jwt_parsed.id},
          {},
          {lean: true},
          (res_err, res_user) => {
            if (res_err || !res_user) return response_parser_helper({status: 401, message: resmessage.auth.unauthorized.message, res});
            delete res_user.password;
            next()
          }
        )

      })
    }

    else next()
};
