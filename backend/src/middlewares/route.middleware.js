const config = require('../../config/config');
const UserSchema = require('../models/schemas/user.schema');

module.exports = (req, res, next) => {

  if (req.method === 'OPTIONS') return res.sendStatus(204);

  if (!config.publicRoutes.includes(req.path.split(config.apiPrefix)[0])) {

    const userId = req.signedCookies['cookie'];
    if (userId) {
      UserSchema.findOne({ _id: userId }).then(user => {

        if (!user) return res.json({ status: 401, message: 'Un-authorized user' });
        else res.cookie('cookie', user._id, { signed: true });
        next();
        
      }).catch(error => res.json({ status: 401, message: error.toString(), error }));

    } else return res.json({ status: 401, message: 'Un-authorized user' });

  } else next();
};
