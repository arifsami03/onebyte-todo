const UserSchema = require('./schemas/user.schema');
const bcryptHelper = require('./../helpers/bcrypt.helper');
module.exports = {

    login(email, password) {
        return new Promise((resolve, reject) => {

            if (!email) return reject({ error: 'Email not provided' });
            else if (!password) return reject({ error: 'Password not provided' });

            UserSchema.findOne({ email }).then(user => {

                if (!user) return reject({ error: 'Invalid credentials' });

                bcryptHelper.bcryptPassCompare(password, user.password).then(matched => {

                    if (!matched) return reject({ error: 'Invalid credentials' });
                    return resolve({ user: user, data: { success: 'You are logged in successfylly' } });

                }).catch(error => {
                    console.log('error comparing password: ', error);
                    return reject({ error: 'Something went wrong, Please try again.' })
                });

            }).catch(error => reject(error));
        })
    },

    signup(user) {
        const userSchema = new UserSchema(user);
        return new UserSchema(userSchema).save()
            .then(savedUser => { return savedUser })
            .catch(error => { return error });
    }
}