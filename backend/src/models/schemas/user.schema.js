
const mongoose = require('mongoose');
const bcryptHelper = require('./../../helpers/bcrypt.helper');

const UserSchema = new mongoose.Schema(
  {

    name: {
      type: String,
      required: [true, 'Name is required']
    },

    email: {
      type: String,
      validate: {
        validator: v => {
          let re = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(v)

        },
        message: 'Invalid email address'
      },
      required: [true, 'Email is required'],
      unique: [true, 'This email already exists']
    },

    password: {
      type: String,
      required: [true, 'Password is required']
    }
  },
  { timestamps: true }
);

UserSchema.pre('save', function (next) {
  if (this.isNew) {
    bcryptHelper.bcryptPassword(this.password)
      .then(hash => {
        this.password = hash;
        this.validateSync();
        next();
      })
      .catch(error => console.log('error in hashing passowrd: ', error));
  } else {
    this.validateSync();
    next();
  }
});


module.exports = mongoose.model('UserSchema', UserSchema);
