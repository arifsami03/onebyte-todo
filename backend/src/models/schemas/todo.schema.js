const mongoose = require('mongoose');

const TodoSchema = new mongoose.Schema(
  {
    todo: {
      type: String,
      required: [true, 'Todo descripton is required']
    },
    completed: Boolean,
    categories: Array,
    userId: {
      type: mongoose.Schema.Types.ObjectId, 
      ref: 'UserSchema',
      required: [true, 'User reference is required']
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('TodoSchema', TodoSchema);
