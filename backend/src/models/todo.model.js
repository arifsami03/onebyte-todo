const TodoSchema = require('./schemas/todo.schema');
module.exports = {

    findAll() {
        return new Promise((resolve, reject) => {
            TodoSchema.find()
                .then(todoList => resolve(todoList))
                .catch(error => reject(error));
        });
    },

    create(todo) {
        const todoSchema = new TodoSchema(todo);
        return new TodoSchema(todoSchema).save()
            .then(savedTodo => { return savedTodo })
            .catch(error => { return error });
    }
}