const express = require('express');
const todoController = require('./../controllers/todo.controller');

const router = express.Router();

router.route('/')
    .get(todoController.findAll)
    .post(todoController.create);

module.exports = router;