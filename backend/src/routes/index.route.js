const express = require('express');
const authRoutes = require('./auth.route');
const todoRoutes = require('./todo.route');

const routeMiddleware = require('../middlewares/route.middleware');


const router = express.Router();

router.use(routeMiddleware);

router.use('/auth', authRoutes);
router.use('/todo', todoRoutes);

module.exports = router;